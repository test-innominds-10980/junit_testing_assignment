package JUnitAssign;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EmployeeSortByNameMainClass 
{
	public ArrayList<EmployeeSortByName> sortByName()
	{
		 ArrayList<EmployeeSortByName> e=new ArrayList<EmployeeSortByName>();
	     e.add(new EmployeeSortByName(20, "Santosh", "python"));
	     e.add(new EmployeeSortByName(30, "Sanjay","ui"));
	     e.add(new EmployeeSortByName(40, "Pramod","dev"));

	     Collections.sort(e, new Comparator<EmployeeSortByName>() {
	         @Override
	         public int compare(EmployeeSortByName o1, EmployeeSortByName o2) {
	             return (int)(o1.getName().compareTo(o2.getName()));
	         }
	     });
	     
	     Collections.sort(e, (o1, o2) -> (o1.getName().compareTo(o2.getName())));
	     System.out.println(e);
		return e;
	}
	
}
