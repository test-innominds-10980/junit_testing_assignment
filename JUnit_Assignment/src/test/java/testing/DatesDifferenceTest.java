package testing;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import JUnitAssign.DatesDifference;
import junit.framework.Assert;

public class DatesDifferenceTest 
{
	   @Test
	    public void datesDifference()
	    {
		   DatesDifference d = new DatesDifference();
	        LocalDate l1 = LocalDate.of(2022,12,31);
	        LocalDate l2 = LocalDate.of(2021,11,30);
	        LocalDate ExpectedDate = LocalDate.of(1,1,1);
	        LocalDate ActualDate = d.differenceDates(l1,l2);
	        assertEquals(ExpectedDate,ActualDate);
	        
	        
	    }
		
}