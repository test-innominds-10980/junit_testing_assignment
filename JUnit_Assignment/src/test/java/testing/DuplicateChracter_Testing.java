package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import JUnitAssign.DuplicateCharacter;

public class DuplicateChracter_Testing {

	@Test
	public void test() 
	{
		DuplicateCharacter d=new DuplicateCharacter();
		assertArrayEquals(new char[] {'i','n'}, d.duplicate1());
	}

}
