package testing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import JUnitAssign.IncomeTaxOfAEmployee;

public class IncomeTaxOfAEmployeeTest {

	IncomeTaxOfAEmployee i=new IncomeTaxOfAEmployee();
	@Test
	public void salaryLessThan50000() 
	{
		assertEquals(0, i.taxPaidByEmployee(40000));
	}

	@Test
	public void salaryLessThan60000() 
	{
		assertEquals(200, i.taxPaidByEmployee(52000));
	}
	
	@Test
	public void salaryLessThan150000() 
	{
		assertEquals(22000, i.taxPaidByEmployee(120000));
	}
	
	@Test
	public void salaryOther() 
	{
		assertEquals(64000, i.taxPaidByEmployee(300000));
	}
}
