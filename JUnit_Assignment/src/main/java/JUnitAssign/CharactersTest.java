package JUnitAssign;

public class CharactersTest
{
	public String checkChar(char a)
	{
		if(Character.isDigit(a))
		{
			return "integer";
		}
		else if(Character.isUpperCase(a))
		{
			return "Uppercase";
		}
		else if(Character.isLowerCase(a))
		{
			return "lowercase";
		}
		else
		{
			return "others";
		}
	}
}
