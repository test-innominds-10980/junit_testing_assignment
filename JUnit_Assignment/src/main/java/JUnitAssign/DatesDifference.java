package JUnitAssign;

import java.time.LocalDate;
import java.time.Period;

public class DatesDifference {
	
	public LocalDate differenceDates(LocalDate l1,LocalDate l2 )
	{
	    int year1 = l1.getYear();
	    int month1 = l1.getMonthValue();
	    int days1 = l1.getDayOfMonth();
	    int year2 = l2.getYear();
	    int month2 = l2.getMonthValue();
	    int days2 = l2.getDayOfMonth();
	    return LocalDate.of(year1-year2, month1-month2, days1-days2);
	}
}