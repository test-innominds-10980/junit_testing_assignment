package JUnitAssign;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController 
{
	@Autowired
	EmployeeRepository e;
	@GetMapping("/employees")
	public List<Employee> listAllEmployees()
	{
		return e.findAll();	
	}
	
	@GetMapping("/employees/{id}")
	public Employee getOneEmployee(@PathVariable("id") Integer id)
	{
		return e.getById(id);	
	}
	
	@PostMapping("/employees")
	public Employee addEmployee(@RequestBody Employee requestEmployee)
	{
		return e.save(requestEmployee);
	}
	
	@PutMapping("/employees/{id}")
	public Employee updateEmployee(@PathVariable("id") Integer id,@RequestBody Employee responseEmployee)
	{
		Employee e1=e.getById(id);
		if(e1!=null)
		{
			e1.setName(responseEmployee.getName());
			e1.setSalary(responseEmployee.getSalary());
			e1.setDesignation(responseEmployee.getDesignation());
		}
		return e.save(e1);
	}
	
	@DeleteMapping("/employees/{id}")
	public String deleteEmployee(@PathVariable("id") Integer id)
	{
		Employee e1=e.getById(id);
		if(e1!=null)
		{
			e.delete(e1);
			return "Employee deleted....";
		}
		else
		{
			return "Employee is not there....";
		}
	}
}
