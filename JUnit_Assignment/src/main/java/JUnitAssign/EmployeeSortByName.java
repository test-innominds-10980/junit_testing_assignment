package JUnitAssign;

public class EmployeeSortByName 
{
	private int id;
	private String name;
	private String department;
	public EmployeeSortByName(int i, String string, String string2) {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	@Override
	public String toString() {
		return "EmployeeSortByName [id=" + id + ", name=" + name + ", department=" + department + "]";
	}
	
}
