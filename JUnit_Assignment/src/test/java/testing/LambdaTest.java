package testing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import JUnitAssign.Lambda;

public class LambdaTest 
{
   @Test
   public void lambdaTest() 
   {
	   Lambda l=new Lambda();
	   assertEquals(30, l.add());
   }
   @Test
   public void normalAdd()
   {
	   Lambda l=new Lambda();
	   assertEquals(40,l.addNormal(20, 20));
	   
   }
}
