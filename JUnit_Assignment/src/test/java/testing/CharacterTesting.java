package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import JUnitAssign.CharactersTest;

public class CharacterTesting {
	CharactersTest c=new CharactersTest();
	@Test
	public void checkLower()
	{
		char a='a';
		assertEquals("lowercase", c.checkChar(a));
	}
	
	@Test
	public void checkUpper()
	{
		char a='A';
		assertEquals("Uppercase", c.checkChar(a));
	}
	
	@Test
	public void checkNumber()
	{
		char a='4';
		assertEquals("integer", c.checkChar(a));
	}
	
	@Test
	public void checkSymbol()
	{
		char a='@';
		assertEquals("others", c.checkChar(a));
	}
	
	@Test
	public void checkSymbol1()
	{
		char a='2';
		assertNotEquals("others", c.checkChar(a));
	}

}
