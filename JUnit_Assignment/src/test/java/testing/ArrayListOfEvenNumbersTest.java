package testing;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import JUnitAssign.ArrayListOfEvenNumbers;

public class ArrayListOfEvenNumbersTest {

	ArrayListOfEvenNumbers a=new ArrayListOfEvenNumbers();
	@Test
	public void evenNumbersAdd()
	{
		a.saveEvenNumbers(10);
		assertNotEquals(a.printEvenNumbers(), a.saveEvenNumbers());
	}
	
	@Test
	public void searchEvenNumber()
	{
		a.saveEvenNumbers(10);
		assertEquals(8, a.printEvenNumber(8));
	}
}
