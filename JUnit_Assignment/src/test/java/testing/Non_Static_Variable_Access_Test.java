

package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import JUnitAssign.Non_Static_Variable_Access;

public class Non_Static_Variable_Access_Test {

	@Test
    public void nonstaticTest() 
	{
		Non_Static_Variable_Access n=new Non_Static_Variable_Access();
        System.out.println(n.nonstatic);
        System.out.println(n.static1);
        assertEquals(5, n.nonstatic);
        assertEquals(5, n.static1);       
    }
    
}


